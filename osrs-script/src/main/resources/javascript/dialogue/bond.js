var entries = new ArrayList();
var title = "";
var lines = new ArrayList();
var actions = new ArrayList();

title = "Select an Option";
lines.add("+14 Days Premium Membership");
actions.add("close|script");
lines.add("Nevermind");
actions.add("close");
var obj0 = new DialogueEntry();
entries.add(obj0);
obj0.setSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

title = "Select an Option";
lines.add("Open Bond Pouch");
actions.add("close|script");
lines.add("Information");
actions.add("close|script");
lines.add("Donator Options");
actions.add("close|dialogue=bond,2");
var obj1 = new DialogueEntry();
entries.add(obj1);
obj1.setLargeSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

title = "Select an Option";
lines.add("Default Skin");
actions.add("close|script");
lines.add("Opal Skin");
actions.add("close|script");
lines.add("Jade Skin");
actions.add("close|script");
lines.add("Topaz Skin");
actions.add("close|script");
lines.add("Sapphire Skin");
actions.add("close|script");
lines.add("Emerald Skin");
actions.add("close|script");
lines.add("Ruby Skin");
actions.add("close|script");
lines.add("Diamond Skin");
actions.add("close|script");
lines.add("Dragonstone Skin");
actions.add("close|script");
lines.add("Onyx Skin");
actions.add("close|script");
var obj2 = new DialogueEntry();
entries.add(obj2);
obj2.setLargeSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

instance = new DialogueScript() {
    execute: function(player, index, childId, slot) {
        if (player.isLocked()) {
            return;
        }
        if (index == 0) {
            if (slot == 0) {
                if (Settings.getInstance().isSpawn()) {
                    return;
                }
                if ( player.getInventory().getCount(ItemId._14_DAYS_PREMIUM_MEMBERSHIP_32303) == 0) {
                    player.getGameEncoder().sendMessage("You need a bond to do this.");
                    return;
                }
                RequestManager.addGoldMembership(player);
                player.setPremiumMember(true);
                player.getGameEncoder().sendMessage("<col=ff0000>14 days of membership have been added to your account.");
                player.getInventory().deleteItem(ItemId._14_DAYS_PREMIUM_MEMBERSHIP_32303, 1);
                player.setPremiumMemberDays(player.isPremiumMemberDays() + 14);
                RequestManager.addPlayerLog(player, "bond", player.getLogName()
                        + " received Premium Membership from a bond.");
                player.getFamiliar().rollPet(ItemId.CHOMPY_CHICK, 1);
            }
        } else if (index == 1) {
            if (slot == 0) {
                player.getBonds().sendPouch();
            } else if (slot == 1) {
                Guide.openEntry(player, "main", "bonds");
            }
        } else if (index == 2) {
            if (slot == 0) {
                player.getAppearance().setColor(4, 1);
            } else if (slot == 1) {
                if (!player.isUsergroup(SqlUserRank.OPAL_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only opal members or above can use this.");
                    return;
                }
                player.getAppearance().setColor(4, 16);
            } else if (slot == 2) {
                if (!player.isUsergroup(SqlUserRank.JADE_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only jade members and above can use this.");
                    return;
                }
                player.getAppearance().setColor(4, 17);
            } else if (slot == 3) {
                if (!player.isUsergroup(SqlUserRank.TOPAZ_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only topaz members and above can use this.");
                    return;
                }
                player.getAppearance().setColor(4, 18);
            } else if (slot == 4) {
                if (!player.isUsergroup(SqlUserRank.SAPPHIRE_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only sapphire members and above can use this.");
                    return;
                }
                player.getAppearance().setColor(4, 19);
            } else if (slot == 5) {
                if (!player.isUsergroup(SqlUserRank.EMERALD_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only emerald members can use this.");
                    return;
                }   
                player.getAppearance().setColor(4, 20);
            } else if (slot == 6) {
                if (!player.isUsergroup(SqlUserRank.RUBY_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only ruby members can use this.");
                    return;
                }   
                player.getAppearance().setColor(4, 21);
            } else if (slot == 7) {
                if (!player.isUsergroup(SqlUserRank.DIAMOND_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only diamond members can use this.");
                    return;
                }   
                player.getAppearance().setColor(4, 22);
            } else if (slot == 8) {
                if (!player.isUsergroup(SqlUserRank.DRAGONSTONE_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only dragonstone members can use this.");
                    return;
                }   
                player.getAppearance().setColor(4, 23);
            } else if (slot == 9) {
                if (!player.isUsergroup(SqlUserRank.ONYX_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only onyx members can use this.");
                    return;
                }   
                player.getAppearance().setColor(4, 24);
            }
        }
    },

    getDialogueEntries: function() {
        return entries;
    }
}
