package com.palidinodh.osrsscript.map.area.slayertower;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 13623, 6727 })
public class SlayerTowerArea extends Area {
}
