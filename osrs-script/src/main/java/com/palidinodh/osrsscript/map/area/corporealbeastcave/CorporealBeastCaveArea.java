package com.palidinodh.osrsscript.map.area.corporealbeastcave;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11844)
public class CorporealBeastCaveArea extends Area {
}
