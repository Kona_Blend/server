package com.palidinodh.osrsscript.map.area.donator.action.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrsscript.map.area.taverley.TaverleyCrystalChest;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.TREASURE_CHEST_18806)
class SuperiorCrystalChestMapObject implements MapObjectHandler {
  @Override
  public void execute(Player player, int option, MapObject mapObject) {
    TaverleyCrystalChest.open(player, mapObject);
  }
}
