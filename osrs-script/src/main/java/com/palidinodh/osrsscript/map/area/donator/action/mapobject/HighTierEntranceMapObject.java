package com.palidinodh.osrsscript.map.area.donator.action.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.SqlUserRank;

@ReferenceId(ObjectId.SHIMMERING_BARRIER_30398)
class HighTierEntranceMapObject implements MapObjectHandler {
  @Override
  public void execute(Player player, int option, MapObject mapObject) {
    if (!player.isUsergroup(SqlUserRank.TOPAZ_MEMBER)
        && !player.isUsergroup(SqlUserRank.SAPPHIRE_MEMBER)
        && !player.isUsergroup(SqlUserRank.EMERALD_MEMBER)) {
      player.getGameEncoder().sendMessage("Your donator rank is not a high enough tier to enter.");
      return;
    }
    player.getMovement().clear();
    if (player.getX() >= 3042) {
      player.getMovement().addMovement(3040, 3503);
    } else {
      player.getMovement().addMovement(3042, 3503);
    }
  }
}
