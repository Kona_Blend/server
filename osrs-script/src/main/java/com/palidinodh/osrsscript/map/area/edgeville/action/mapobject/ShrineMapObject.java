package com.palidinodh.osrsscript.map.area.edgeville.action.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.model.player.skill.SkillContainer;
import com.palidinodh.osrsscript.player.skill.Woodcutting;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.SHRINE)
class ShrineMapObject implements MapObjectHandler {
  @Override
  public void execute(Player player, int option, MapObject mapObject) {
    SkillContainer.getContainer(Woodcutting.class).checkShrine(player);
  }
}
