package com.palidinodh.osrsscript.map.area.edgeville.action.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.LOYALTY_MANAGER)
class LoyaltyManagerNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.openDialogue("loyalty", 0);
  }
}
