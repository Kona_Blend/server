package com.palidinodh.osrsscript.map.area.dwarvernmine.action.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BELONA)
class BelonaNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (option == 3) {
      player.getSkills().setMiningMinerals(!player.getSkills().getMiningMinerals());
      player.getGameEncoder()
          .sendMessage("Minerals while mining: " + player.getSkills().getMiningMinerals());
    } else {
      player.openShop("skilling");
    }
  }
}
