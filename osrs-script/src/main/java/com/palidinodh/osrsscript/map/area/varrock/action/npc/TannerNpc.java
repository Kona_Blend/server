package com.palidinodh.osrsscript.map.area.varrock.action.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.model.player.skill.SkillContainer;
import com.palidinodh.osrsscript.player.skill.Crafting;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.TANNER)
class TannerNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    SkillContainer.getContainer(Crafting.class).openTanning(player);
  }
}
