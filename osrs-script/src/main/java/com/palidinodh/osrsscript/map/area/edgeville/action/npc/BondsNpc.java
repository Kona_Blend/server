package com.palidinodh.osrsscript.map.area.edgeville.action.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.CAPT_BOND_16018)
class BondsNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.getBonds().sendPouch();
  }
}
