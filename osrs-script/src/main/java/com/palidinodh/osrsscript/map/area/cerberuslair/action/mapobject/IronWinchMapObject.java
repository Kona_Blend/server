package com.palidinodh.osrsscript.map.area.cerberuslair.action.mapobject;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(ObjectId.IRON_WINCH)
class IronWinchMapObject implements MapObjectHandler {
  @Override
  public void execute(Player player, int option, MapObject mapObject) {
    if (!Settings.getInstance().isSpawn()
        && !player.getSkills().isAnySlayerTask(NpcId.CERBERUS_318)) {
      player.getGameEncoder().sendMessage("You need an appropriate task to enter.");
      return;
    }
    if (mapObject.getX() == 1291 && mapObject.getY() == 1254) {
      player.getMovement().teleport(1240, 1227);
    } else if (mapObject.getX() == 1328 && mapObject.getY() == 1254) {
      player.getMovement().teleport(1368, 1227);
    } else if (mapObject.getX() == 1307 && mapObject.getY() == 1269) {
      player.openDialogue("bossinstance", 4);
    }
  }
}
