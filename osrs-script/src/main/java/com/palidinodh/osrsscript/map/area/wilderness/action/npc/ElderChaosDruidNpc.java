package com.palidinodh.osrsscript.map.area.wilderness.action.npc;

import com.palidinodh.osrscore.io.ValueEnteredEvent;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.model.player.Prayer;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;
import lombok.var;

@ReferenceId(NpcId.ELDER_CHAOS_DRUID)
class ElderChaosDruidNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    openDialogue(player, player.getInventory().getSlotByNameIgnoreCase("bone"));
  }

  @Override
  public void execute(Player player, int widgetId, int childId, int slot, Npc npc) {
    if (widgetId != WidgetId.INVENTORY) {
      return;
    }
    openDialogue(player, slot);
  }

  private void openDialogue(Player player, int slot) {
    var item = player.getInventory().getItem(slot);
    if (item == null || item.getDef().getUnnotedId() == -1) {
      return;
    }
    if (!Prayer.isBone(item.getDef().getUnnotedId())) {
      return;
    }
    var exchangeCount = player.getInventory().getCount(item.getId());
    exchangeCount = Math.min(exchangeCount, player.getInventory().getRemainingSlots());
    exchangeCount = Math.min(exchangeCount, Item.MAX_AMOUNT / 50);
    player.openDialogue(new OptionsDialogue(DialogueOption.toOptions(
        "Exchange '" + ItemDef.getName(item.getDef().getUnnotedId()) + "': 50 coins",
        "Exchange 5: 250 coins",
        "Exchange All: " + PNumber.formatNumber(exchangeCount * 50) + " coins", "Exchange X",
        "Cancel")).action((c, s) -> unnote(player, slot)));
  }

  private void unnote(Player player, int slot) {
    var item = player.getInventory().getItem(slot);
    if (item == null || item.getDef().getUnnotedId() == -1) {
      return;
    }
    var amount = 0;
    if (slot == 0) {
      amount = 1;
    } else if (slot == 1) {
      amount = 5;
    } else if (slot == 2) {
      amount = Item.MAX_AMOUNT;
    }
    var valueEntered = new ValueEnteredEvent.IntegerEvent() {
      @Override
      public void execute(int value) {
        var itemId = player.getInventory().getId(slot);
        if (itemId == -1 || value == 0) {
          return;
        }
        value = Math.min(value, player.getInventory().getCount(itemId));
        value = Math.min(value, player.getInventory().getRemainingSlots());
        value = Math.min(value, Item.MAX_AMOUNT / 50);
        if (player.getInventory().getCount(ItemId.COINS) < value * 50) {
          player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
          return;
        }
        player.getInventory().deleteItem(ItemId.COINS, value * 50);
        player.getInventory().deleteItem(itemId, value);
        player.getInventory().addItem(ItemDef.getUnnotedId(itemId), value);
      }
    };
    if (slot == 3) {
      player.getGameEncoder().sendEnterAmount(valueEntered);
    } else {
      valueEntered.execute(amount);
    }
  }
}
