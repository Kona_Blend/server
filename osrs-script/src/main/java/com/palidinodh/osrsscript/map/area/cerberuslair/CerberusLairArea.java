package com.palidinodh.osrsscript.map.area.cerberuslair;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 4883, 5139, 5140, 5395 })
public class CerberusLairArea extends Area {
}
