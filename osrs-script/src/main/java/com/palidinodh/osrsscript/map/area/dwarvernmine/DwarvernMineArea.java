package com.palidinodh.osrsscript.map.area.dwarvernmine;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11929, 12183, 12184, 12185 })
public class DwarvernMineArea extends Area {
}
