package com.palidinodh.osrsscript.map.area.wilderness.action.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.PILES)
class PilesNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.getGameEncoder().sendMessage("Piles will note items for you.");
  }

  @Override
  public void execute(Player player, int widgetId, int childId, int slot, Npc npc) {
    if (widgetId != WidgetId.INVENTORY) {
      return;
    }
    player.getInventory().noteItems(slot, 50);
  }
}
