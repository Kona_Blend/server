package com.palidinodh.osrsscript.map.area.fishingguild;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10293)
public class FishingGuildArea extends Area {
}
