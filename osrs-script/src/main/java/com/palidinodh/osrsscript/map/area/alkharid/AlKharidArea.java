package com.palidinodh.osrsscript.map.area.alkharid;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

// 13106 is also the Duel Arena.
// 13362 is also the Duel Arena.
// 13363 is also the Duel Arena and Mage Training Arena.
@ReferenceId({ 13105, 13106, 13107, 13362, 13363 })
public class AlKharidArea extends Area {
}
