package com.palidinodh.osrsscript.map.area.warriorsguild;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11319)
public class WarriorsGuildArea extends Area {
}
