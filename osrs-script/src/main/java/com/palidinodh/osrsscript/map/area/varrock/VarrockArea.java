package com.palidinodh.osrsscript.map.area.varrock;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

// 12596 is also the Champions' Guild.
// 12597 is also the Cooks' Guild.
// 12598 is also the Grand Exchange.
// 13110 is also the Lumber Yard.
@ReferenceId({ 12596, 12597, 12598, 12852, 12853, 12854, 13108, 13109, 13110 })
public class VarrockArea extends Area {
}
