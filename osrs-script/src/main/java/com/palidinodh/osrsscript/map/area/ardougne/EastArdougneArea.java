package com.palidinodh.osrsscript.map.area.ardougne;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10547)
public class EastArdougneArea extends Area {
}
