package com.palidinodh.osrsscript.map.area.krakencove;

import com.palidinodh.osrscore.model.map.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9116)
public class KrakenCoveArea extends Area {
}
