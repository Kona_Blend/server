package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.VORKATH_8059)
class VorkathNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (npc.isLocked()) {
      return;
    }
    npc.setTransformationId(8061);
    npc.setAnimation(7950);
    npc.setLock(8);
  }
}
