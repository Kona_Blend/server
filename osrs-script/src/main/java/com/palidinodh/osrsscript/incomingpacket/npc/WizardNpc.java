package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.model.player.Teleports;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WIZARD_4399)
class WizardNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (option == 0) {
      Teleports.open(player);
    } else if (option == 2) {
      int[] teleportIndices = player.getWidgetManager().getLastTeleport(0);
      Teleports.destinationWidgetPressed(player, teleportIndices[0], teleportIndices[1]);
    }
  }
}
