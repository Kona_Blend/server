package com.palidinodh.osrsscript.incomingpacket.command;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.setting.SqlUserRank;
import lombok.var;

class PrivateCommand implements CommandHandler {
  @Override
  public String getExample() {
    return "username";
  }

  @Override
  public boolean canUse(Player player) {
    return player.isUsergroup(SqlUserRank.TRIAL_MODERATOR)
        || player.getRights() == Player.RIGHTS_MOD || player.getRights() == Player.RIGHTS_ADMIN
        || player.isUsergroup(SqlUserRank.COMMUNITY_MANAGER);
  }

  @Override
  public void execute(Player player, String username) {
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    } else if (player2.getController().isInstanced()) {
      player.getGameEncoder().sendMessage(username + " is in an instance located at: "
          + player.getX() + ", " + player.getY() + ", " + player.getHeight() + ".");
      return;
    } else if ((player.getController().inWilderness() || player.getController().inPvPWorld())
        && !(player.getRights() == Player.RIGHTS_ADMIN)) {
      player.getGameEncoder().sendMessage(username + " is in wilderness and can't be moved.");
      RequestManager.addPlayerLog("commands/0.txt", player.getLogName() + " tried to move "
          + player2.getLogName() + " to private from the wilderness.");
      return;
    }
    player2.getGameEncoder().sendMessage(player.getUsername() + " has moved you.");
    player.getGameEncoder().sendMessage(username + " has been moved.");
    player2.getMovement().teleport(2895, 2727);
    player2.getController().stopWithTeleport();
    RequestManager.addPlayerLog("commands/0.txt",
        player.getLogName() + " teleported " + player2.getLogName() + " to private.");
  }
}
