package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Farming;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ NpcId.TOOL_LEPRECHAUN, NpcId.TOOL_LEPRECHAUN_757, NpcId.TOOL_LEPRECHAUN_7757 })
class ToolLeprechaunNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.getGameEncoder().sendMessage("The leprechaun will note items for you.");
  }
  @Override
  public void execute(Player player, int widgetId, int childId, int slot, Npc npc) {
    if (widgetId != WidgetId.INVENTORY) {
      return;
    }
    var item = player.getInventory().getItem(slot);
    if (item == null || item.getDef().getNotedId() == -1) {
      return;
    }
    var notedId = item.getDef().getNotedId();
    if (!Farming.isCollectable(item.getId()) || notedId == -1) {
      return;
    }
    var exchangeCount2 = player.getInventory().getCount(item.getId());
    player.getInventory().deleteItem(item.getId(), exchangeCount2);
    player.getInventory().addItem(notedId, exchangeCount2);
  }
}
