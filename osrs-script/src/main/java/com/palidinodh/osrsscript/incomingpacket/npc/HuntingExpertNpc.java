package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.Tile;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Magic;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.HUNTING_EXPERT_1504)
class HuntingExpertNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (npc.getX() == 3508 && npc.getY() == 3479) {
      player.getMovement().animatedTeleport(new Tile(3530, 3444),
          Magic.NORMAL_MAGIC_ANIMATION_START, Magic.NORMAL_MAGIC_ANIMATION_END,
          Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
    } else if (npc.getX() == 2644 && npc.getY() == 3662) {
      player.getMovement().animatedTeleport(new Tile(2720, 3781),
          Magic.NORMAL_MAGIC_ANIMATION_START, Magic.NORMAL_MAGIC_ANIMATION_END,
          Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
    } else {
      player.openShop("skilling");
    }
  }
}
