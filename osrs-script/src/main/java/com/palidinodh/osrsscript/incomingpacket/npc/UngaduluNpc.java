package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.Tile;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.UNGADULU_70)
class UngaduluNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (player.getCombat().getLegendsQuest() == 0 && player.carryingItem(730)
        && player.getWorld().getTargetNPC(3962, player) == null) {
      player.getGameEncoder()
          .sendMessage("You open the book and a light starts emanating, illuminating Ungadulu.");
      player.getPrayer().adjustPoints(-99);
      Npc nezikchened =
          new Npc(player.getController(), 3962, new Tile(2792, 9328, player.getHeight()));
      nezikchened.setForceMessage("Your faith will help you little here.");
      nezikchened.getCombat().setTarget(player);
    } else if (player.getCombat().getLegendsQuest() == 2 && player.carryingItem(746)) {
      player.getInventory().deleteItem(746, 1);
      player.getInventory().addItem(748, 1);
      player.getMovement().teleport(2792, 9337, 0);
      player.getGameEncoder()
          .sendMessage("Ungadulu gives you a water purifying spell in exchange for the dagger.");
    } else {
      player.getGameEncoder().sendMessage("You try to speak to Ungadulu, but he ignores you.");
    }
  }
}
