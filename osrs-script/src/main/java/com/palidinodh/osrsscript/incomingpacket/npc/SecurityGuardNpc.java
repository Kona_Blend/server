package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SECURITY_GUARD)
class SecurityGuardNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.getInventory().addOrDropItem(9003, 1);
  }
}
