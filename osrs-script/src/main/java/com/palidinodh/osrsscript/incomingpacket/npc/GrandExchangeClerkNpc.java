package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.adaptive.GrandExchangeUser;

@ReferenceId(NpcId.GRAND_EXCHANGE_CLERK)
class GrandExchangeClerkNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (option == 0) {
      player.openDialogue("grandexchange", 0);
    } else if (option == 2) {
      player.getGrandExchange().open();
    } else if (option == 3) {
      player.getGrandExchange().openHistory(GrandExchangeUser.HISTORY);
    } else if (option == 4) {
      player.getGrandExchange().exchangeItemSets();
    }
  }
}
