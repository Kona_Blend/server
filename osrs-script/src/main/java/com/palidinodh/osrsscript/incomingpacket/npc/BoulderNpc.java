package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BOULDER_3967)
class BoulderNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (player.getCombat().getLegendsQuest() == 2) {
      player.getGameEncoder()
          .sendMessage("You search around the rock and discover a dagger on the ground.");
      player.getGameEncoder().sendMessage("Ungadulu might be able to do something with this.");
      player.getInventory().addOrDropItem(746, 1);
    }
  }
}
