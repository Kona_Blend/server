package com.palidinodh.osrsscript.incomingpacket.command;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrsscript.world.event.holidayboss.HolidayBossEvent;

class HolidayBossCommand implements CommandHandler {
  @Override
  public void execute(Player player, String message) {
    player.getWorld().getWorldEvent(HolidayBossEvent.class).teleport(player);
  }
}
