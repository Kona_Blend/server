package com.palidinodh.osrsscript.incomingpacket.command;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrsscript.world.event.holidayboss.HolidayBossEvent;
import com.palidinodh.osrsscript.world.event.holidayboss.HolidayBossType;
import com.palidinodh.rs.setting.SqlUserRank;
import lombok.var;

class ChristmasCommand implements CommandHandler {
  @Override
  public boolean canUse(Player player) {
    return player.getRights() != Player.RIGHTS_NONE
        || player.isUsergroup(SqlUserRank.COMMUNITY_MANAGER);
  }

  @Override
  public void execute(Player player, String command) {
    var event = player.getWorld().getWorldEvent(HolidayBossEvent.class);
    if (event.getType() != null) {
      event.setType(null);
      player.getGameEncoder().sendMessage("Holiday event disabled.");
      player.getWorld().sendBroadcast("The holiday event has ended!");
    } else {
      event.setType(HolidayBossType.ANTI_SANTA);
      player.getGameEncoder().sendMessage("Anti-Santa event enabled.");
      player.getWorld().sendBroadcast("The holiday event has been enabled!");
    }
  }
}
