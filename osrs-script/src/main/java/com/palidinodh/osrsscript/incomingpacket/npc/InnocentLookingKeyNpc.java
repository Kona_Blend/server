package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.Tile;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.INNOCENT_LOOKING_KEY)
class InnocentLookingKeyNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (player.getCombat().getHauntedMine() < 2
        || player.getWorld().getTargetNPC(3616, player) != null) {
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
      return;
    } else if (player.getCombat().getHauntedMine() == 2) {
      Npc treus = new Npc(player.getController(), 3616, new Tile(2788, 4457, player.getHeight()));
      treus.getCombat().setTarget(player);
    } else if (player.getCombat().getHauntedMine() >= 3) {
      player.getInventory().addItem(4077, 1);
    }
  }
}
