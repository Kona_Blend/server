package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WYSON_THE_GARDENER)
class WysonTheGardenerNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    int[] moleItemIds = new int[] { 7416, 7417, 7418, 7419 };
    for (int itemId : moleItemIds) {
      int count = Math.min(player.getInventory().getCount(itemId),
          player.getInventory().getRemainingSlots());
      player.getInventory().deleteItem(itemId, count);
      for (int i = 0; i < count; i++) {
        if (PRandom.randomE(10) == 0) {
          player.getInventory().addItem(5075, 1);
        } else if (PRandom.randomE(5) == 0) {
          player.getInventory().addItem(5074, 1);
        } else {
          player.getInventory().addItem(7413, 1);
        }
      }
    }
  }
}
