package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ NpcId.BABY_IMPLING, NpcId.YOUNG_IMPLING, NpcId.GOURMET_IMPLING, NpcId.EARTH_IMPLING,
    NpcId.ESSENCE_IMPLING, NpcId.ECLECTIC_IMPLING, NpcId.NATURE_IMPLING, NpcId.MAGPIE_IMPLING,
    NpcId.NINJA_IMPLING, NpcId.DRAGON_IMPLING, NpcId.BABY_IMPLING_1645, NpcId.YOUNG_IMPLING_1646,
    NpcId.GOURMET_IMPLING_1647, NpcId.EARTH_IMPLING_1648, NpcId.ESSENCE_IMPLING_1649,
    NpcId.ECLECTIC_IMPLING_1650, NpcId.NATURE_IMPLING_1651, NpcId.MAGPIE_IMPLING_1652,
    NpcId.NINJA_IMPLING_1653, NpcId.DRAGON_IMPLING_1654, NpcId.LUCKY_IMPLING,
    NpcId.LUCKY_IMPLING_7302, NpcId.CRYSTAL_IMPLING, NpcId.CRYSTAL_IMPLING_8742,
    NpcId.CRYSTAL_IMPLING_8743, NpcId.CRYSTAL_IMPLING_8744, NpcId.CRYSTAL_IMPLING_8745,
    NpcId.CRYSTAL_IMPLING_8746, NpcId.CRYSTAL_IMPLING_8747, NpcId.CRYSTAL_IMPLING_8748,
    NpcId.CRYSTAL_IMPLING_8749, NpcId.CRYSTAL_IMPLING_8750, NpcId.CRYSTAL_IMPLING_8751,
    NpcId.CRYSTAL_IMPLING_8752, NpcId.CRYSTAL_IMPLING_8753, NpcId.CRYSTAL_IMPLING_8754,
    NpcId.CRYSTAL_IMPLING_8755, NpcId.CRYSTAL_IMPLING_8756, NpcId.CRYSTAL_IMPLING_8757 })
class ImplingNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.getHunter().catchNPC(npc);
  }
}
