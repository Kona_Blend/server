package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Magic;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.TYSS)
class TyssNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (option == 0) {
      player.openDialogue("spellbooks", 0);
    } else if (option == 2) {
      player.getMagic().setVengeanceCast(false);
      if (player.getMagic().getSpellbook() == Magic.STANDARD_MAGIC) {
        player.getMagic().setSpellbook(Magic.ANCIENT_MAGIC);
      } else if (player.getMagic().getSpellbook() == Magic.ANCIENT_MAGIC) {
        player.getMagic().setSpellbook(Magic.LUNAR_MAGIC);
      } else {
        player.getMagic().setSpellbook(Magic.STANDARD_MAGIC);
      }
    }
  }
}
