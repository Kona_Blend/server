package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SQUIRE_1764)
class SquireNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    if (option == 0) {
      player.openDialogue("bank", 1);
    } else if (option == 2) {
      player.getBank().open();
    }
  }
}
