package com.palidinodh.osrsscript.incomingpacket;

import java.util.HashMap;
import java.util.Map;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Stream;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.AchievementDiary;
import com.palidinodh.osrscore.model.player.Hunter;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.model.player.skill.SkillContainer;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import lombok.var;

class NpcOptionDecoder extends IncomingPacketDecoder {
  static Map<Integer, NpcHandler> handlers = new HashMap<>();

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var npcIndex = getInt(InStreamKey.TARGET_INDEX);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    player.clearIdleTime();
    var npc = player.getWorld().getNpcByIndex(npcIndex);
    if (npc == null) {
      return false;
    }
    var message = "[NpcOption(" + option + ")] index=" + npcIndex + "; ctrlRun=" + ctrlRun + "; id="
        + npc.getId() + "/" + NpcId.valueOf(npc.getId());
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().getPrintPackets()) {
      player.getGameEncoder().sendMessage(message);
    }
    RequestManager.addUserPacketLog(player, message);
    if (player.isLocked()) {
      return false;
    }
    if (player.getMovement().isViewing()) {
      return false;
    }
    player.clearAllActions(false, true);
    player.setFaceEntity(npc);
    if (npc.getMoveRange() == null && !npc.getDef().getLCName().contains("fishing")) {
      player.getMovement().setFollowFront(npc);
    } else {
      player.getMovement().setFollowing(npc);
    }
    if (option == 1 && npc.getMaxHitpoints() > 0) {
      player.setAttacking(true);
      player.setInteractingEntity(npc);
      player.getCombat().setFollowing(npc);
      player.getMovement().follow();
      if (player.getMagic().getAutoSpellId() != 0 && player.getHitDelay() <= 0) {
        player.setHitDelay(2);
      }
      return false;
    }
    player.getMovement().follow();
    return true;
  }

  @Override
  public boolean complete(Player player) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var npcIndex = getInt(InStreamKey.TARGET_INDEX);
    var npc = player.getWorld().getNpcByIndex(npcIndex);
    if (npc == null) {
      return true;
    }
    var range = 1;
    if (npc.getDef().hasOption("bank")) {
      range = 2;
    }
    if (player.getMovement().isRouting() && npc.getMovement().isRouting()) {
      range++;
    }
    if (player.isLocked()) {
      return false;
    }
    if (!player.withinDistance(npc, range)) {
      return false;
    }
    if (!npc.getMovement().isRouting() && npc.getSizeX() == 1 && player.getX() != npc.getX()
        && player.getY() != npc.getY()) {
      return false;
    }
    if (Hunter.getHuntedNPC(npc.getId()) != null && !player.getHunter().catchNPCStage(npc)) {
      return false;
    }
    player.setFaceEntity(null);
    player.getMovement().setFollowing(null);
    if (player.getX() == npc.getX() || player.getY() == npc.getY()) {
      player.getMovement().clear();
    }
    player.setFaceTile(npc);
    AchievementDiary.npcOptionUpdate(player, option, npc);
    if (player.getController().npcOptionHook(option, npc)) {
      return true;
    }
    if (SkillContainer.npcOptionHooks(player, option, npc)) {
      return true;
    }
    if (npc.getDef().isOption(option, "pick-up") && npc == player.getFamiliar().getFamiliar()) {
      player.getFamiliar().removeFamiliar();
      return true;
    }
    var handler = player.getArea().getNpcHandler(npc.getId());
    if (handler == null) {
      handler = handlers.get(npc.getId());
    }
    if (handler != null) {
      handler.execute(player, option, npc);
      return true;
    }
    if (basicAction(player, option, npc)) {
      return true;
    }
    player.getGameEncoder().sendMessage("Nothing interesting happens.");
    return true;
  }

  private static boolean basicAction(Player player, int option, Npc npc) {
    switch (npc.getDef().getLCName()) {
      case "banker":
      case "ghost banker":
      case "gnome banker":
        if (option == 0) {
          player.openDialogue("bank", 1);
        } else if (option == 2) {
          player.getBank().open();
        }
        return true;
    }
    return false;
  }

  static {
    try {
      var classes = Readers.getScriptClasses(NpcHandler.class, "incomingpacket.npc");
      for (var clazz : classes) {
        var handler = Readers.newInstance(clazz);
        var ids = NpcHandler.getIds(handler);
        if (ids == null) {
          continue;
        }
        for (var id : ids) {
          if (handlers.containsKey(id)) {
            throw new RuntimeException(clazz.getName() + " - " + id + ": npc id already used.");
          }
          handlers.put(id, handler);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }
}
