package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.RUBY_HARVEST)
class RubyHarvestNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.getHunter().catchNPC(npc);
  }
}
