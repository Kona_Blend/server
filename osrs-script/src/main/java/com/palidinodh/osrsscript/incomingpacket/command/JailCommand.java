package com.palidinodh.osrsscript.incomingpacket.command;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.setting.SqlUserRank;
import lombok.var;

class JailCommand implements CommandHandler {
  @Override
  public String getExample() {
    return "username";
  }

  @Override
  public boolean canUse(Player player) {
    return player.isUsergroup(SqlUserRank.TRIAL_MODERATOR)
        || player.getRights() == Player.RIGHTS_MOD || player.getRights() == Player.RIGHTS_ADMIN
        || player.isUsergroup(SqlUserRank.COMMUNITY_MANAGER);
  }

  @Override
  public void execute(Player player, String username) {
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    } else if (player2.getController().isInstanced()) {
      player.getGameEncoder().sendMessage(username + " is in an instance located at: "
          + player.getX() + ", " + player.getY() + ", " + player.getHeight() + ".");
      return;
    } else if ((player2.getController().inWilderness() || player2.getController().inPvPWorld())
        && !(player.getRights() == Player.RIGHTS_ADMIN)) {
      player.getGameEncoder().sendMessage("The player you are trying to move is in wilderness.");
      RequestManager.addPlayerLog("commands/0.txt", player.getLogName() + " tried to teleport "
          + player2.getLogName() + " out from the wilderness.");
    }
    player2.getMovement().teleport(2094, 4466);
    player2.getGameEncoder().sendMessage("You have been jailed by " + player.getUsername());
    player.getGameEncoder().sendMessage(username + " has been jailed.");
    player.getWorld()
        .sendStaffMessage(player.getUsername() + " has jailed " + player2.getUsername() + ".");
    RequestManager.addPlayerLog("commands/0.txt",
        player.getLogName() + " teleported " + player2.getLogName() + " to jail.");

  }
}
