package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.npc.Npc;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.LILLY)
class LillyNpc implements NpcHandler {
  @Override
  public void execute(Player player, int option, Npc npc) {
    player.openShop("warriors_guild_potions");
  }
}
