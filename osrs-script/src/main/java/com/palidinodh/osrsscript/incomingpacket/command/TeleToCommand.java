package com.palidinodh.osrsscript.incomingpacket.command;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.player.Player;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.setting.SqlUserRank;
import lombok.var;

class TeleToCommand implements CommandHandler {
  @Override
  public String getExample() {
    return "username";
  }

  @Override
  public boolean canUse(Player player) {
    return player.getRights() == Player.RIGHTS_MOD || player.getRights() == Player.RIGHTS_ADMIN
        || player.isUsergroup(SqlUserRank.COMMUNITY_MANAGER);
  }

  @Override
  public void execute(Player player, String username) {
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    } else if (player == player2) {
      player.getGameEncoder().sendMessage("You can't teleport to yourself.");
      return;
    } else if (player.getController().isInstanced()) {
      player.getGameEncoder().sendMessage("You can't teleport while in an instance.");
      return;
    } else if (player2.getController().isInstanced()) {
      player.getGameEncoder().sendMessage(username + " is in an instance located at: "
          + player2.getX() + ", " + player2.getY() + ", " + player2.getHeight() + ".");
      return;
    } else if ((player.getController().inWilderness() || player.getController().inPvPWorld())
        && !(player.getRights() == Player.RIGHTS_ADMIN)) {
      player.getGameEncoder().sendMessage("You can't teleport out of wilderness.");
      RequestManager.addPlayerLog("commands/0.txt", player.getLogName() + " tried to teleport to "
          + player2.getLogName() + " from the wilderness.");
      return;
    } else if ((player2.getController().inWilderness() || player2.getController().inPvPWorld())
        && !(player.getRights() == Player.RIGHTS_ADMIN)) {
      player.getGameEncoder()
          .sendMessage("The player you are trying to teleport to is in the wilderness.");
      RequestManager.addPlayerLog("commands/0.txt", player.getLogName() + " tried to teleport to "
          + player2.getLogName() + " in the wilderness.");
    }
    player.getMovement().teleport(player2);
    RequestManager.addPlayerLog("commands/0.txt",
        player.getLogName() + " teleported to " + player2.getLogName() + ".");
  }
}
